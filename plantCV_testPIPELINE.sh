#!/bin/bash

MAIN_DIR=$1
PARALLEL=$2
cd $MAIN_DIR

if [ $PARALLEL -eq 0 ]
	then
	# main loop: looping across tray folders
	for tray in $(echo T*/)
	do
	TRAY=$(echo ${MAIN_DIR}/${tray})
	mkdir ${TRAY}/PLANT_CV_OUTPUT/
	mkdir ${TRAY}/RAINBOW_CLUSTERS/

	# EXTRACT LEAF AREAS PER DATE (or picture) FOR A TRAY (or tray folder)
	cd $TRAY
	for pic_filename in $(ls *.JPG)
	do
	ENTRY=$(echo $pic_filename | cut -d'.' -f1)
	echo $ENTRY
	# separate individual plants prior to leaf area measurement
	${MAIN_DIR}/separate_PLANTS.py \
	-i $pic_filename \
	-o ${TRAY}/PLANT_CV_OUTPUT/ \
	-n ${MAIN_DIR}/*.csv \
	-D None
	mv 10_clusters.png ${TRAY}/RAINBOW_CLUSTERS/${ENTRY}-CLUSTER.png
	rm *.png
	# compute leaf areas for each plant picture
	cd ${TRAY}/PLANT_CV_OUTPUT/
	touch ${ENTRY}_AREAS.temp
	for i in $(ls *jpg)
	do
	${MAIN_DIR}/extract_AREA.py ${i}
	cat plantCV.out >> ${ENTRY}_AREAS.temp
	rm plantCV.out
	echo ${i}
	done
	rm *.jpg
	cd $TRAY
	done
	#end-leaf area extraction

	# CONSOLIDATE LEAF AREAS PER DATE INTO ONE TRAY FILE
	cd ${TRAY}/PLANT_CV_OUTPUT/
	ls *_AREAS.temp > ALL.AREAS.temp
	###################  ### ### ### ### ### ### ### ###  ###################
	#saving areas file per image for Ted's manual missing plant annotations:
	for ted in $(cat ALL.AREAS.temp)
	do
	mv $ted $(echo $ted | cut -d'.' -f1).txt
	done
	###################  ### ### ### ### ### ### ### ###  ###################
	OUT=$(echo $TRAY | rev | cut -d'/' -f2 | rev)
	echo -e "TRAY\tDATE\tCELL\tAREA" > ${MAIN_DIR}/${OUT}_AREAS.txt
	for file in $(cat ALL.AREAS.temp)
	do
	# extract tray number and date of measurement
	echo $file | cut -d'_' -f1 > tray.temp
	echo $file | cut -d'_' -f2 > day.temp
	echo $file | cut -d'_' -f3 > month.temp
	echo $file | cut -d'_' -f4 > year.temp
	for i in $(seq $(($(cat $file | wc -l) -1)))
	do
	cp tray.temp t.temp; head -n1 t.temp >> tray.temp
	cp day.temp d.temp; head -n1 d.temp >> day.temp
	cp month.temp m.temp; head -n1 m.temp >> month.temp
	cp year.temp y.temp; head -n1 y.temp >> year.temp
	done
	# extract cell number and areas
	cut -d$'\t' -f1 $file > cell_raw.temp
	cut -d'_' -f3 cell_raw.temp > cell.temp
	cut -d$'\t' -f2 $file > area.temp
	# paste by column the corresponding tray, dates, cell and area columns
	paste -d$'-' year.temp month.temp day.temp > date.temp
	paste -d$'\t' tray.temp date.temp cell.temp area.temp > out.temp
	# append to the main file for the tray across time
	cat out.temp >> ${MAIN_DIR}/${OUT}_AREAS.txt
	done
	#end-consolidation
	rm *.temp
	done
	#end-main loop
elif [ $PARALLEL -eq 1 ]
	then
	parallel ./parallelPlantCV_testPIPELINE.sh $MAIN_DIR {} ::: $(echo T*/)
else
	echo "Nope! That's an illegat input!"
fi
