#!/usr/bin/python

import sys, traceback
import cv2
import os
import re
import numpy as np
import argparse
import string

# _dir_='/'.join(os.getcwd().split('/')[0:-2])
_dir_= os.getcwd()
sys.path.insert(0, _dir_ + '/plantcv')
import plantcv as pcv

IMAGE = sys.argv[1]

debug=None
device=0

img, path, filename = pcv.readimage(IMAGE)
device, a = pcv.rgb2gray_lab(img, 'a', device, debug)
device, img_binary = pcv.binary_threshold(a, 123, 255, 'dark', device, debug)
device, id_objects, obj_hierarchy = pcv.find_objects(img, img_binary, device, debug)
device, roi, roi_hierarchy = pcv.define_roi(img, 'rectangle', device, None, 'default', debug)
device, roi_objects, roi_obj_hierarchy, kept_mask, obj_area = pcv.roi_objects(img, 'partial', roi, roi_hierarchy, id_objects, obj_hierarchy, device, debug)

OUT = np.column_stack((IMAGE, obj_area))
np.savetxt("plantCV.out", OUT, fmt='%s', delimiter="\t")
