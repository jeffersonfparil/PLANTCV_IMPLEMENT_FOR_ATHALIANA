# PLANT COMPUTER VISION IMPLEMENTATION FOR Arabidopsis thaliana

A suite of python, R and Bash scripts for the analysis of leaf growth of Arabidopsis thaliana from pictures

## Install Dependencies:
```
sudo apt install python python-numpy python-pip python-tk parallel
```
```
pip install numpy opencv-python scikit-image

```

## Prepare the Input Images:
* [1] Make sure the green square fillers do not overlap with one another!
* [2] Rotate the pictures such that there are 7 rows and 6 columns (A to F)
* [3] Prepare the TRAY FOLDERS (FOLDER NAMES SHOULD START WITH THE LETTER "T") one folder for each tray containing all pictures of that tray through time
* [4] Create a MAIN DIRECTORY containing all the TRAY FOLDERS plus a csv file containing the cell names (extension name .csv)
* [5] Copy the plantcv/ directory into the MAIN DIRECTORY
* [6] Copy into the MAIN DIRECTORY AND SET AS EXECUTABLES THE FOLLOWING:
	* [6a] separate_PLANTS.py
	* [6b] extract_AREA.py
	* [6c] parallelPlantCV_testPIPELINE.sh
	* [6d] plantCV_testPIPELINE.sh
	```
	chmod +x *.py *.sh
	```
* [7] Run the shell script in Bash via:
	```
	nohup ./plantCV_testPIPELINE.sh /full/path/to/MAIN/DIRECTORY/ 1 &
	```
	Example:
	```   
	nohup ./plantCV_testPIPELINE.sh /home/ubuntu/PLANTCV_IMPLEMENT_FOR_ATHALIANA/ 0 &
	```
	* [ARGUMENTS:]
		* [1] Main directory containing all T*/ folders
		* [2] Parallelize? ==> 0 for no and 1 for yes!
## Data Processing:
misc/ folder contains a test R script to group the areas by dates
