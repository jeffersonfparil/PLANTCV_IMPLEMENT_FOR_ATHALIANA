dat = read.table("T1_AREAS.txt", header=T)

#group area by dates::: cells across rows and dates across columns
for (date in levels(dat$DATE)) {
	print(date)
	x = subset(dat, DATE==date)
	X = as.data.frame(x$AREA); rownames(X) = x$CELL; colnames(X)=date

	OUT = tryCatch(merge(OUT, X, by="row.names"), error=function(e) return(X))
	print(OUT)
	if (ncol(OUT) > 2) {row.names(OUT) = OUT$Row.names; OUT = OUT[,2:ncol(OUT)]}
}

write.csv(OUT, file="OUT.csv")